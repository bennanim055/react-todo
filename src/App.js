import './App.css';
import { useState, createContext } from 'react';
import Items from './components/Items';


export const AppContext = createContext(null);

function App() {

  const [item,setItem]=useState();
  const [list,setList]=useState([{id:"1",item:"salade",active:"false"},{id:"2",item:"cornichons",active:'false'}]);

  const add=(e)=>{
    e.preventDefault();
    setList([...list,{id: Math.random().toString(),item:item,active:'false'}])
    setItem("");
  }

    

  return (
    <AppContext.Provider value={{ list, setList }}>

    <div className="App">
      <h1>Liste de courses</h1>
      
      <Items />


      <form onSubmit={add}>
        <h3>Ajouter un nouvel item à la liste:</h3>
        <textarea value={item} onChange={(e)=>setItem(e.target.value)} />
        <button type="submit">Ajouter</button>
      </form>

    </div>
    </AppContext.Provider>

  );
}

export default App;

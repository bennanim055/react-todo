import React, { useContext } from 'react'


import { AppContext } from "../App";

function Item({item}) {
    const { list, setList } = useContext(AppContext);

    
  const checkThis=(e)=>{

    let newList = list.map((l)=>{
      if(l.id===e.target.value){
        if(l.active==="false"){
          l.active="true";
        }else{
          l.active="false";
        }
      }
      return l
    })
    
    setList(newList);
}
  return (
    <li key={item.id}>
          <input type="checkbox" value={item.id} onClick={checkThis} />
      <p className={item.active} >{(item.item)}</p>
      </li>
  )
}

export default Item